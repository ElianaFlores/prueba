package ar.edu.unju.fi.model;

public class Compra {
	private int id;
	private Producto producto;
	private int cantidad;
	public Compra(int id, Producto producto, int cantidad) {
		super();
		this.id = id;
		this.producto = producto;
		this.cantidad = cantidad;
	}
	@Override
	public String toString() {
		return "Compra []";
	}
	
}
